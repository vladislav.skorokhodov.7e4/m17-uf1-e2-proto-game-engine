﻿using System;
namespace GameTools
{

    /*
     * GameEngine:
     * Protype to print sequence of frames in console.
     * 
     * To stop de main while press ESC key.
     * 
     */


    public class GameEngine
    {

        public Char[,] mat = new char[10, 10] {
                { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' },
                { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' },
                { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' },
                { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' },
                { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' },
                { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' },
                { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' },
                { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' },
                { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' },
                { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' }, };

        public Char[,] Copy = new char[10, 10];

        public int comptador = 0;


        //Declaració d'una variable
        private ConsoleColor _backgroundConsoleColor;
        //Declaració d'una propietat
        public ConsoleColor BackgroundConsoleColor
        {
            get { return _backgroundConsoleColor; }
            set
            {
                if (ConsoleColor.White != value || ConsoleColor.Black != value) _backgroundConsoleColor = value;
                else
                {
                    _backgroundConsoleColor = ConsoleColor.Gray;
                    throw new ArgumentException($"Console color {value} not recomended. Set by default");
                }
            }
        }

        private int time2liveframe;
        private float _frameRate;
        public float FrameRate
        {
            get { return _frameRate; }
            set
            {
                //Ternari condition
                _frameRate = (value < 0f) ? value * (-1f) : value;
            }
        }


        //Declaració d'una propietat no protegida
        public int Frames { get; set; }

        private ConsoleKeyInfo cki;
        private bool engineSwitch;

        public GameEngine()
        {
            InitGame();
            UpdateGame();
            CloseGame();
        }


        private void InitGame()
        {
            /*** Init variables ***/

            Frames = 0;
            engineSwitch = false;

            //Acces exemple with this:
            this._frameRate = (_frameRate <= 0) ? 12 : _frameRate;

            //Calculate the frame time in miliseconds. Time to refresh. F=1/s ->s=1/F
            time2liveframe = (int)((1 / _frameRate) * 1000);

            /*******/

            //Prepare Console
            CleanFrame();
            Console.BackgroundColor = _backgroundConsoleColor;

            Console.WriteLine("\nPress a key to display; press the ESC key to quit.");

            Console.WriteLine($"Game Initiation             Render data: Framerate: {_frameRate} || TimeToRefresh:{time2liveframe}");

            Start();

            System.Threading.Thread.Sleep(2000);
        }

        /*
         * Engine updates every frame
         * 
         * Reprint console
         */
        private void UpdateGame()
        {

            do
            {

                while (Console.KeyAvailable == false)
                {
                   
                    CleanFrame();

                    Update();

                    CheckKeyboard4Engine();

                    //Console.WriteLine(engineSwitch);

                    RefreshFrame();

                    Frames++;
                }

                cki = Console.ReadKey(true);

            } while (engineSwitch);

        }

        private void ListenKeyboard()
        {
            cki = Console.ReadKey();
        }

        private void CheckKeyboard4Engine()
        {
            engineSwitch = (cki.Key == ConsoleKey.Escape);
        }

        private void RefreshFrame()
        {
            //Access to Threading library only in this line
            System.Threading.Thread.Sleep(time2liveframe);
        }

        private void CloseGame()
        {

            Console.WriteLine("You pressed the '{0}' key.", cki.Key);
            Exit();
            Console.WriteLine(" Game Over. Closing game");
        }


        private void CleanFrame()
        {
            Console.Clear();
            Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop+1);
        }

        protected void Start()
        {
            //Code before first frame
        }

        protected void Update()
        {
            Random random = new Random();

            int columna = random.Next(0, 9);

            for (int i = 0; i < mat.GetLength(0); i++)
            {
                for (int j = 0; j < mat.GetLength(1); j++)
                {
                    Console.Write("{0} ", mat[i, j]);
                }
                Console.WriteLine();
            }

            for (int x = Copy.GetLength(0) - 1; x >= 0; x--)
            {
                for (int y = 0; y < Copy.GetLength(1); y++)
                {
                    int asciiValue = random.Next(65, 90);
                    Char asciiChar = Convert.ToChar(asciiValue);
                    if (x != 0)
                    {
                        Copy[x, y] = mat[x - 1, y];
                    }
                    else
                    {
                        if (columna == y)
                        {
                            Copy[x, y] = asciiChar;
                        }
                        else
                        {
                            Copy[x, y] = '0';
                        }
                    }


                }
            }
            mat = Copy;

        }

        protected void Exit()
        {
            //Code afer last frame
        }

        public void bajada(Char[,] uno, Char[,] dos, Char lletra, int rnd)
        {
            for (int x = dos.GetLength(0) - 1; x >= 0; x--)
            {
                for (int y = 0; y < dos.GetLength(1); y++)
                {
                    dos[x, y] = (x != 0) ? uno[x - 1, y] : (rnd < 25) ? lletra : '0';
                }
            }
            uno = dos;
        }

    }

}


public class ConsoleSpiner
{
    int counter;
    public ConsoleSpiner()
    {
        counter = 0;
    }
    public void Turn()
    {
        counter++;
        switch (counter % 4)
        {
            case 0: Console.Write("/"); break;
            case 1: Console.Write("-"); break;
            case 2: Console.Write("\\"); break;
            case 3: Console.Write("|"); break;
        }
        //Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
    }
}
